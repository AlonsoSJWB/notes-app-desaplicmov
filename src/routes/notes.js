const express = require('express')
const router = express.Router()
const NoteModel = require('../models/Note')

router.get('/notes/add', (req, res)=>{
    res.render('notes/new-note')
})

router.get('/notes', async(req, res)=>{
    await NoteModel.find({}).sort({creationDate: 'desc'}).then(concepts =>{
        const ctx = {
            notes: concepts.map(concept=>{
                return {
                    _id: concept._id,
                    title: concept.title,
                    description: concept.description
                } 
            })
        }
        res.render('notes/all-notes', {notes: ctx.notes})
    })
})

router.post('/notes/new-note', async (req, res)=>{
    const { title, description } = req.body
    const errors = []
    //console.log("Note body", req.body)
    if(!title){
        errors.push({text: "Please write a title"})
    }
    if(!description){
        errors.push({text: "Please write a description"})
    }
    if(errors.length > 0){
        res.render('notes/new-note', {
            errors,
            title,
            description
        })
    }else {
        const newNote = new NoteModel({title, description})
        await newNote.save()
        res.redirect('/notes')
    }
})

router.get('/notes/edit/:id', async (req, res)=>{
    const noteDB = await NoteModel.findById(req.params.id)
    const note = {
        _id: noteDB._id,
        title: noteDB.title,
        description: noteDB.description,
        creationDate: noteDB.creationDate
    }
    res.render('notes/edit-note', {note})
})

router.put('/notes/edit-note/:id', async (req, res)=>{
    const { title, description } = req.body
    await NoteModel.findByIdAndUpdate(req.params.id, { title, description })
    res.redirect('/notes')
})

router.delete('/notes/delete/:id', async(req, res)=>{
    await NoteModel.findByIdAndDelete(req.params.id)
    res.redirect('/notes')
})


module.exports = router